

<div class="w-full" x-data="window.xRiqSKhIlmnKtWu" x-init="initialize()" @keydown.window.escape="open = false" x-cloak>

    
    {{-- Mobile filters --}}
    <div x-show="open" class="fixed inset-0 flex z-40 lg:hidden" x-ref="dialog" aria-modal="true">
    
        <div x-show="open" x-transition:enter="transition-opacity ease-linear duration-300" x-transition:enter-start="opacity-0" x-transition:enter-end="opacity-100" x-transition:leave="transition-opacity ease-linear duration-300" x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0" x-description="Off-canvas menu overlay, show/hide based on off-canvas menu state." class="fixed inset-0 bg-black bg-opacity-25" @click="open = false" aria-hidden="true" style="display: none;">
        </div>

    
        <div x-show="open" x-transition:enter="transition ease-in-out duration-300 transform" x-transition:enter-start="translate-x-full" x-transition:enter-end="translate-x-0" x-transition:leave="transition ease-in-out duration-300 transform" x-transition:leave-start="translate-x-0" x-transition:leave-end="translate-x-full" class="ml-auto relative max-w-xs w-full h-full bg-white dark:bg-zinc-800 shadow-xl py-4 pb-12 flex flex-col overflow-y-auto" style="display: none;">

            <div class="px-4 flex items-center justify-between">
                <h2 class="text-lg font-medium text-gray-900 dark:text-gray-200">{{ __('messages.t_filter') }}</h2>
                <button type="button" class="ltr:-mr-2 rtl:-ml-2 w-10 h-10 bg-white dark:bg-zinc-700 p-2 rounded-md flex items-center justify-center text-gray-400 dark:text-gray-300" @click="open = false">
                <span class="sr-only">Close menu</span>
                <svg class="h-6 w-6" x-description="Heroicon name: outline/x" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path>
                </svg>
                </button>
            </div>

            {{-- Filter --}}
            <div class="mt-4 border-t border-gray-200 dark:border-zinc-700">

                {{-- Rating --}}
                <div x-data="{ open: true }" class="py-3">
                    <h3 class="-my-3 flow-root bg-gray-50 dark:bg-zinc-700 px-4">
                        <button @click="open = !open" type="button" class="py-3 w-full flex items-center justify-between text-sm text-gray-400 hover:text-gray-500 outline-none focus:outline-none">
                            <span class="font-medium text-gray-900">{{ __('messages.t_rating') }}</span>
                            <span class="ltr:ml-6 rtl:mr-6 flex items-center">
                                <svg class="h-5 w-5" x-description="Expand icon, show/hide based on section open state. Heroicon name: solid/plus-sm" x-show="!(open)" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path fill-rule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clip-rule="evenodd"></path> </svg>
                                <svg class="h-5 w-5" x-description="Collapse icon, show/hide based on section open state. Heroicon name: solid/minus-sm" x-show="open" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true" style="display: none;"> <path fill-rule="evenodd" d="M5 10a1 1 0 011-1h8a1 1 0 110 2H6a1 1 0 01-1-1z" clip-rule="evenodd"></path> </svg>
                            </span>
                        </button>
                    </h3>
                    <div class="pt-6 px-4" x-show="open" style="display: none;">
                        <div class="space-y-4">

                            {{-- 5 stars --}}
                            <div class="flex items-center">
                                <input wire:model.defer="rating" id="filter-rating-5" name="rating" value="5" type="radio" class="h-4 w-4 border-gray-300 rounded-full text-primary-600 focus:ring-primary-600">
                                <label for="filter-rating-5" class="ltr:ml-3 rtl:mr-3 text-sm text-gray-600">
                                    <div class="flex items-center">
                                        @for ($i = 0; $i < 5; $i++)
                                            <svg class="text-amber-400 flex-shrink-0 h-5 w-5" x-description="Heroicon name: solid/star" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path> </svg>
                                        @endfor
                                    </div>
                                </label>
                            </div>

                            {{-- 4 stars --}}
                            <div class="flex items-center">
                                <input wire:model.defer="rating" id="filter-rating-4" name="rating" value="4" type="radio" class="h-4 w-4 border-gray-300 rounded-full text-primary-600 focus:ring-primary-600">
                                <label for="filter-rating-4" class="ltr:ml-3 rtl:mr-3 text-sm text-gray-600">
                                    <div class="flex items-center">
                                        @for ($i = 0; $i < 4; $i++)
                                            <svg class="text-amber-400 flex-shrink-0 h-5 w-5" x-description="Heroicon name: solid/star" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path> </svg>
                                        @endfor
                                        <svg class="text-gray-300 flex-shrink-0 h-5 w-5" x-description="Heroicon name: solid/star" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path> </svg>
                                    </div>
                                </label>
                            </div>

                            {{-- 3 stars --}}
                            <div class="flex items-center">
                                <input wire:model.defer="rating" id="filter-rating-3" name="rating" value="3" type="radio" class="h-4 w-4 border-gray-300 rounded-full text-primary-600 focus:ring-primary-600">
                                <label for="filter-rating-3" class="ltr:ml-3 rtl:mr-3 text-sm text-gray-600">
                                    <div class="flex items-center">
                                        @for ($i = 0; $i < 3; $i++)
                                            <svg class="text-amber-400 flex-shrink-0 h-5 w-5" x-description="Heroicon name: solid/star" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path> </svg>
                                        @endfor
                                        @for ($i = 0; $i < 2; $i++)
                                            <svg class="text-gray-300 flex-shrink-0 h-5 w-5" x-description="Heroicon name: solid/star" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path> </svg>
                                        @endfor
                                    </div>
                                </label>
                            </div>

                            {{-- 2 stars --}}
                            <div class="flex items-center">
                                <input wire:model.defer="rating" id="filter-rating-2" name="rating" value="2" type="radio" class="h-4 w-4 border-gray-300 rounded-full text-primary-600 focus:ring-primary-600">
                                <label for="filter-rating-2" class="ltr:ml-3 rtl:mr-3 text-sm text-gray-600">
                                    <div class="flex items-center">
                                        @for ($i = 0; $i < 2; $i++)
                                            <svg class="text-amber-400 flex-shrink-0 h-5 w-5" x-description="Heroicon name: solid/star" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path> </svg>
                                        @endfor
                                        @for ($i = 0; $i < 3; $i++)
                                            <svg class="text-gray-300 flex-shrink-0 h-5 w-5" x-description="Heroicon name: solid/star" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path> </svg>
                                        @endfor
                                    </div>
                                </label>
                            </div>

                            {{-- 1 stars --}}
                            <div class="flex items-center">
                                <input wire:model.defer="rating" id="filter-rating-1" name="rating" value="1" type="radio" class="h-4 w-4 border-gray-300 rounded-full text-primary-600 focus:ring-primary-600">
                                <label for="filter-rating-1" class="ltr:ml-3 rtl:mr-3 text-sm text-gray-600">
                                    <div class="flex items-center">
                                        @for ($i = 0; $i < 1; $i++)
                                            <svg class="text-amber-400 flex-shrink-0 h-5 w-5" x-description="Heroicon name: solid/star" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path> </svg>
                                        @endfor
                                        @for ($i = 0; $i < 4; $i++)
                                            <svg class="text-gray-300 flex-shrink-0 h-5 w-5" x-description="Heroicon name: solid/star" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path> </svg>
                                        @endfor
                                    </div>
                                </label>
                            </div>
                        
                        </div>
                    </div>
                </div>

                {{-- Price --}}
                <div x-data="{ open: true }" class="py-3">
                    <h3 class="-my-3 flow-root bg-gray-50 dark:bg-zinc-700 px-4">
                        <button @click="open = !open" type="button" class="py-3 w-full flex items-center justify-between text-sm text-gray-400 hover:text-gray-500 outline-none focus:outline-none">
                            <span class="font-medium text-gray-900">{{ __('messages.t_price') }}</span>
                            <span class="ltr:ml-6 rtl:mr-6 flex items-center">
                                <svg class="h-5 w-5" x-description="Expand icon, show/hide based on section open state. Heroicon name: solid/plus-sm" x-show="!(open)" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path fill-rule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clip-rule="evenodd"></path> </svg>
                                <svg class="h-5 w-5" x-description="Collapse icon, show/hide based on section open state. Heroicon name: solid/minus-sm" x-show="open" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true" style="display: none;"> <path fill-rule="evenodd" d="M5 10a1 1 0 011-1h8a1 1 0 110 2H6a1 1 0 01-1-1z" clip-rule="evenodd"></path> </svg>
                            </span>
                        </button>
                    </h3>
                    <div class="pt-6 px-4" x-show="open" style="display: none;">
                        <div class="space-y-4">

                            <div class="rounded-md shadow-sm -space-y-px">
                                <div>
                                    <input type="integer" wire:model.defer="min_price" minlength="1" min="1" maxlength="999999999" max="999999999" class="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 dark:placeholder-gray-200 dark:text-gray-100 text-gray-900 rounded-t-md focus:outline-none focus:ring-primary-600 focus:border-primary-600 focus:z-10 sm:text-sm font-medium dark:bg-transparent dark:border-zinc-600" placeholder="{{ __('messages.t_min_price') }}">
                                </div>
                                <div>
                                    <input type="integer" wire:model.defer="max_price" minlength="1" min="1" maxlength="999999999" max="999999999" required="" class="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 dark:placeholder-gray-200 dark:text-gray-100 text-gray-900 rounded-b-md focus:outline-none focus:ring-primary-600 focus:border-primary-600 focus:z-10 sm:text-sm font-medium dark:bg-transparent dark:border-zinc-600" placeholder="{{ __('messages.t_max_price') }}">
                                </div>
                            </div>
                        
                        </div>
                    </div>
                </div>

                {{-- Delivery time --}}
                <div x-data="{ open: true }" class="py-3">
                    <h3 class="-my-3 flow-root bg-gray-50 dark:bg-zinc-700 px-4">
                        <button @click="open = !open" type="button" class="py-3 w-full flex items-center justify-between text-sm text-gray-400 hover:text-gray-500 outline-none focus:outline-none">
                            <span class="font-medium text-gray-900">{{ __('messages.t_delivery_time') }}</span>
                            <span class="ltr:ml-6 rtl:mr-6 flex items-center">
                                <svg class="h-5 w-5" x-description="Expand icon, show/hide based on section open state. Heroicon name: solid/plus-sm" x-show="!(open)" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path fill-rule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clip-rule="evenodd"></path> </svg>
                                <svg class="h-5 w-5" x-description="Collapse icon, show/hide based on section open state. Heroicon name: solid/minus-sm" x-show="open" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true" style="display: none;"> <path fill-rule="evenodd" d="M5 10a1 1 0 011-1h8a1 1 0 110 2H6a1 1 0 01-1-1z" clip-rule="evenodd"></path> </svg>
                            </span>
                        </button>
                    </h3>
                    <div class="pt-6 px-4" x-show="open" style="display: none;">
                        <div class="space-y-4">

                            @foreach ($delivery_times as $key => $time)
                                <div class="flex items-center">
                                    <input wire:model.defer="delivery_time" id="filter-delivery-time-{{ $key }}" value="{{ $time['value'] }}" name="delivery_time" type="radio" class="focus:ring-primary-600 h-4 w-4 text-primary-600 border-gray-300">
                                    <label for="filter-delivery-time-{{ $key }}" class="ltr:ml-3 rtl:mr-3 block text-sm font-medium text-gray-700 dark:text-gray-300">
                                        {{ $time['text'] }}
                                    </label>
                                </div>
                            @endforeach
                        
                        </div>
                    </div>
                </div>

                {{-- Action buttons --}}
                <div class="py-6 px-4">

                    {{-- Filter --}}
                    <x-forms.button action="filter" :text="__('messages.t_filter')" :block="true" />

                    {{-- Reset --}}
                    
                    @if ($rating || $delivery_time || $min_price || $max_price)
                        <span wire:click="resetFilter" class="hover:underline text-xs font-medium text-gray-600 hover:text-gray-800 mt-4 text-center block cursor-pointer">{{ __('messages.t_reset_filter') }}</span>
                        
                    @endif
                    
                </div>

            </div>

        </div>
    
    </div>
    
    {{-- Category Container --}}
    <main class="px-4 sm:px-6 lg:px-8">

        {{-- Section title --}}
       

        {{-- Section content --}}
          <nav class="hidden justify-between px-4 py-3 text-gray-700 rounded-lg sm:flex sm:px-5 bg-white dark:bg-zinc-700/40 dark:border-zinc-700 shadow" aria-label="Breadcrumb">

        
        <ol class="inline-flex items-center mb-3 space-x-1 md:space-x-3 rtl:space-x-reverse sm:mb-0">

            
            <li>
                <div class="flex items-center">
                    <a href="../../index.html" class="ltr:ml-1 rtl:mr-1 text-sm font-medium text-gray-700 hover:text-primary-600 ltr:md:ml-2 rtl:md:mr-2 dark:text-zinc-300 dark:hover:text-white">
                        Home                    </a>
                </div>
            </li>

            
            <li aria-current="page">
                <div class="flex items-center">
                    <svg aria-hidden="true" class="w-5 h-5 text-gray-400 rtl:rotate-180" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd"></path></svg>
                    <a href="../../explore/projects.html" class="ltr:ml-1 rtl:mr-1 text-sm font-medium text-gray-700 hover:text-primary-600 ltr:md:ml-2 rtl:md:mr-2 dark:text-zinc-300 dark:hover:text-white">
                         Post Work                    </a>

                </div>
            </li>

            
            <li aria-current="page">
                <div class="flex items-center">
                    <svg aria-hidden="true" class="w-5 h-5 text-gray-400 rtl:rotate-180" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd"></path></svg>
                    <span class="mx-1 text-sm font-medium text-gray-500 md:mx-2 dark:text-zinc-500 truncate max-w-[18rem]">
                        Post Work Description
                    </span>
                </div>
            </li>

        </ol>

        
        <div class="relative">
           <!--<div class="dropdown">-->
           <!--   <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">-->
           <!--     Dropdown button-->
           <!--   </button>-->
           <!--   <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">-->
           <!--     <a class="dropdown-item" href="#">Action</a>-->
           <!--     <a class="dropdown-item" href="#">Another action</a>-->
           <!--     <a class="dropdown-item" href="#">Something else here</a>-->
           <!--   </div>-->
           <!-- </div>-->
        </div>

    </nav>

    <div class="py-0 sm:py-10">
        <div class="gap-6 grid grid-cols-1 lg:grid-cols-3 lg:grid-flow-col-dense sm:mt-8">

			
			<div class="space-y-10 lg:col-start-1 lg:col-span-2">
				

				
				<section class="w-full">
					<div class="bg-white dark:bg-zinc-800 shadow-sm rounded-lg border dark:border-transparent">

						
						<div class="py-3.5 px-4 w-full rounded-t-lg flex items-center sm:justify-between bg-gray-50 dark:bg-zinc-700 h-16 space-y-3 sm:space-y-0">
							<div>
								<h2 class="text-sm font-bold tracking-wide text-gray-600 dark:text-white">Project details</h2>
							</div>

							
							
						</div>

						
						<div class="border-t border-gray-100 dark:border-zinc-600 px-4 py-5 sm:px-6 pb-8">

							
							<h2 class="mb-4 font-bold text-zinc-600 dark:text-zinc-300 text-sm">
							    
								Description
							</h2>  
							<div class="text-base font-normal leading-relaxed text-zinc-500 dark:text-zinc-200 mb-12 block break-all" style="word-break: break-word;">
								<section id="main-carousel" class="splide rounded-xl">
                            <div class="splide__track">
                                    <ul class="splide__list">
                                        <li class="splide__slide dark:bg-zinc-700 bg-gray-200 object-contain" data-splide-youtube="https://www.youtube.com/watch?v=-BvfSmswLrE">
                                            <img src="{{asset('public/img/auth/login.svg')}}" alt="Teach or assist you with creating ios apps in swiftui" class=" object-contain">
                                        </li>
                                    </ul>
                            </div>
                        </section>
							</div>

							
							<h2 class="mb-4 font-bold text-zinc-600 dark:text-zinc-300 text-sm">
								Client info    
							</h2> 
							<div class="mb-12 block">
								<div class="flex items-center space-x-4 rtl:space-x-reverse">
									<img class="w-10 h-10 rounded-md lazy object-cover" src="../../public/storage/site/placeholder/3A094BE8F9E5093D0205.webp" data-src="https://riverr.edhub.me/public/storage/avatars/A23C05822C0F1EB551E1.png" alt="My Test Pro">
									<div class="font-medium text-zinc-800 dark:text-white">
										<div class="text-[13px] mb-1">Al***oe</div>
										<div class="text-xs text-gray-500 font-normal dark:text-gray-400">
											The client's account will appear to you if he contacts you										</div>
									</div>
								</div>
							</div>

							
							<div>
								<h2 class="mb-4 font-bold text-zinc-600 dark:text-zinc-300 text-sm">Required skills</h2>  
																	<a class="bg-gray-100 hover:bg-primary-100 text-gray-800 hover:text-primary-600 text-xs font-medium ltr:mr-2 rtl:ml-2 ltr:first:mr-0 rtl:first:ml-0 px-2.5 py-1 rounded dark:bg-zinc-700 dark:text-zinc-300 border border-gray-500 dark:hover:border-zinc-400 hover:border-primary-600 transition-colors duration-200 mb-2 inline-block" href="../../explore/projects/website-design/vuejs.html">
										VueJs
									</a>   
																	<a class="bg-gray-100 hover:bg-primary-100 text-gray-800 hover:text-primary-600 text-xs font-medium ltr:mr-2 rtl:ml-2 ltr:first:mr-0 rtl:first:ml-0 px-2.5 py-1 rounded dark:bg-zinc-700 dark:text-zinc-300 border border-gray-500 dark:hover:border-zinc-400 hover:border-primary-600 transition-colors duration-200 mb-2 inline-block" href="../../explore/projects/website-design/react.html">
										React
									</a>   
																	<a class="bg-gray-100 hover:bg-primary-100 text-gray-800 hover:text-primary-600 text-xs font-medium ltr:mr-2 rtl:ml-2 ltr:first:mr-0 rtl:first:ml-0 px-2.5 py-1 rounded dark:bg-zinc-700 dark:text-zinc-300 border border-gray-500 dark:hover:border-zinc-400 hover:border-primary-600 transition-colors duration-200 mb-2 inline-block" href="../../explore/projects/website-design/javascript.html">
										Javascript
									</a>   
																	<a class="bg-gray-100 hover:bg-primary-100 text-gray-800 hover:text-primary-600 text-xs font-medium ltr:mr-2 rtl:ml-2 ltr:first:mr-0 rtl:first:ml-0 px-2.5 py-1 rounded dark:bg-zinc-700 dark:text-zinc-300 border border-gray-500 dark:hover:border-zinc-400 hover:border-primary-600 transition-colors duration-200 mb-2 inline-block" href="../../explore/projects/website-design/html.html">
										HTML
									</a>   
																	<a class="bg-gray-100 hover:bg-primary-100 text-gray-800 hover:text-primary-600 text-xs font-medium ltr:mr-2 rtl:ml-2 ltr:first:mr-0 rtl:first:ml-0 px-2.5 py-1 rounded dark:bg-zinc-700 dark:text-zinc-300 border border-gray-500 dark:hover:border-zinc-400 hover:border-primary-600 transition-colors duration-200 mb-2 inline-block" href="../../explore/projects/website-design/css.html">
										CSS
									</a>   
								                              
							</div>

							
															<div class="rounded-md bg-yellow-50 dark:bg-zinc-700 p-4 mt-10">
									<div class="flex">
										<div class="flex-shrink-0">
											<svg class="h-5 w-5 text-yellow-400 dark:text-zinc-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path fill-rule="evenodd" d="M8.257 3.099c.765-1.36 2.722-1.36 3.486 0l5.58 9.92c.75 1.334-.213 2.98-1.742 2.98H4.42c-1.53 0-2.493-1.646-1.743-2.98l5.58-9.92zM11 13a1 1 0 11-2 0 1 1 0 012 0zm-1-8a1 1 0 00-1 1v3a1 1 0 002 0V6a1 1 0 00-1-1z" clip-rule="evenodd"/> </svg>
										</div>
										<div class="ltr:ml-3 rtl:mr-3">
											<h3 class="text-sm font-medium text-yellow-800 dark:text-zinc-100">Beware of scams</h3>
											<div class="mt-2 text-sm text-yellow-700 dark:text-zinc-300">
												<p>
													If you are being asked to pay a security deposit, or if you are being asked to chat on Telegram, WhatsApp, or another messaging platform, it is likely a scam. Report these projects or contact Support for assistance.      
												</p>
											</div>
										</div>
									</div>
								</div>
							
						</div>
					</div>
				</section>

				
				<section class="relative">

					
					<div wire:loading wire:target="filter" wire:loading.block>
						<div class="absolute w-full h-full flex items-center justify-center bg-black bg-opacity-50 z-50 rounded-lg">
							<div class="lds-ripple"><div></div><div></div></div>
						</div>
					</div>

			
					
					
				</section>
		
			</div>
    
            
            <section class="lg:col-start-3 lg:col-span-1">

                
                <div class="overflow-hidden mb-6 bg-white dark:bg-zinc-800 shadow-sm rounded-lg border dark:border-transparent">

                    
                    <div class="px-4 py-5 sm:px-6 bg-gray-50 dark:bg-zinc-700 h-auto md:h-16 flex items-center">
						<h2 class="text-sm font-bold tracking-wide text-gray-600 dark:text-white">
                            Project summary                        </h2>
                    </div>

					
                    <div class="border-t border-gray-100 dark:border-zinc-600">
                        <dl>

                            
                            <div class="bg-white dark:bg-zinc-700 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                <dt class="text-sm font-medium text-gray-500 dark:text-zinc-300">Budget</dt>
                                <dd class="mt-1 text-sm text-zinc-900 dark:text-white sm:col-span-2 sm:mt-0 font-semibold">
                                    $20.00 – $50.00                                </dd>
                            </div>

							
							<div class="bg-gray-50 dark:bg-zinc-600 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                <dt class="text-sm font-medium text-gray-500 dark:text-zinc-300">Budget type</dt>
                                <dd class="mt-1 text-sm text-zinc-900 dark:text-white sm:col-span-2 sm:mt-0 font-semibold">
                                    										Fixed price									                                </dd>
                            </div>
							
							
							<div class="bg-white dark:bg-zinc-700 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
								<dt class="text-sm font-medium text-gray-500 dark:text-zinc-300">Bids</dt>
								<dd class="mt-1 text-sm text-gray-900 dark:text-white sm:col-span-2 sm:mt-0 font-semibold">
									0
								</dd>
							</div>

							
							<div class="bg-gray-50 dark:bg-zinc-600 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
								<dt class="text-sm font-medium text-gray-500 dark:text-zinc-300">Avg bid</dt>
								<dd class="mt-1 text-sm text-gray-900 dark:text-white sm:col-span-2 sm:mt-0 font-semibold">
									$45.00								</dd>
							</div>

							
							<div class="bg-white dark:bg-zinc-700 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
								<dt class="text-sm font-medium text-gray-500 dark:text-zinc-300">Status</dt>
								<dd class="mt-1 text-sm text-gray-900 dark:text-white sm:col-span-2 sm:mt-0">
									
																				<span class="bg-emerald-100 text-emerald-600 mt-1 inline-block sm:mt-0 sm:rounded-3xl py-2 px-5 font-semibold text-xs">
												Active											</span>
											
								</dd>
							</div>

							
							<div class="bg-gray-50 dark:bg-zinc-600 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
								<dt class="text-sm font-medium text-gray-500 dark:text-zinc-300">Clicks</dt>
								<dd class="mt-1 text-sm text-gray-900 dark:text-white sm:col-span-2 sm:mt-0 font-semibold">
									146
								</dd>
							</div>

							
							<div class="bg-white dark:bg-zinc-700 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
								<dt class="text-sm font-medium text-gray-500 dark:text-zinc-300">Impressions</dt>
								<dd class="mt-1 text-sm text-gray-900 dark:text-white sm:col-span-2 sm:mt-0 font-semibold">
									200
								</dd>
							</div>

							
							<div class="bg-gray-50 dark:bg-zinc-600 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
								<dt class="text-sm font-medium text-gray-500 dark:text-zinc-300">Posted date</dt>
								<dd class="mt-1 text-sm text-gray-900 dark:text-white sm:col-span-2 sm:mt-0 font-semibold">
									5 months ago
								</dd>
							</div>
							
                      	</dl>
                    </div>

				</div>

          </section>
        </div>
    </div>

    </main>
</div>

@push('scripts')

    {{-- AlpineJs --}}
    <script>
        function xRiqSKhIlmnKtWu() {
            return {

                open: false,

                // Init component
                initialize() {
                    
                }

            }
        }
        window.xRiqSKhIlmnKtWu = xRiqSKhIlmnKtWu();
    </script>

@endpush