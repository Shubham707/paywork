

<div class="w-full" x-data="window.xRiqSKhIlmnKtWu" x-init="initialize()" @keydown.window.escape="open = false" x-cloak>

    
    
    <div x-show="open" class="fixed inset-0 flex z-40 lg:hidden" x-ref="dialog" aria-modal="true">
    
        <div x-show="open" x-transition:enter="transition-opacity ease-linear duration-300" x-transition:enter-start="opacity-0" x-transition:enter-end="opacity-100" x-transition:leave="transition-opacity ease-linear duration-300" x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0" x-description="Off-canvas menu overlay, show/hide based on off-canvas menu state." class="fixed inset-0 bg-black bg-opacity-25" @click="open = false" aria-hidden="true" style="display: none;">
        </div>

    
        <div x-show="open" x-transition:enter="transition ease-in-out duration-300 transform" x-transition:enter-start="translate-x-full" x-transition:enter-end="translate-x-0" x-transition:leave="transition ease-in-out duration-300 transform" x-transition:leave-start="translate-x-0" x-transition:leave-end="translate-x-full" class="ml-auto relative max-w-xs w-full h-full bg-white dark:bg-zinc-800 shadow-xl py-4 pb-12 flex flex-col overflow-y-auto" style="display: none;">

            <div class="px-4 flex items-center justify-between">
                <h2 class="text-lg font-medium text-gray-900 dark:text-gray-200"><?php echo e(__('messages.t_filter')); ?></h2>
                <button type="button" class="ltr:-mr-2 rtl:-ml-2 w-10 h-10 bg-white dark:bg-zinc-700 p-2 rounded-md flex items-center justify-center text-gray-400 dark:text-gray-300" @click="open = false">
                <span class="sr-only">Close menu</span>
                <svg class="h-6 w-6" x-description="Heroicon name: outline/x" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path>
                </svg>
                </button>
            </div>

            
            <div class="mt-4 border-t border-gray-200 dark:border-zinc-700">

                
                <div x-data="{ open: true }" class="py-3">
                    <h3 class="-my-3 flow-root bg-gray-50 dark:bg-zinc-700 px-4">
                        <button @click="open = !open" type="button" class="py-3 w-full flex items-center justify-between text-sm text-gray-400 hover:text-gray-500 outline-none focus:outline-none">
                            <span class="font-medium text-gray-900"><?php echo e(__('messages.t_rating')); ?></span>
                            <span class="ltr:ml-6 rtl:mr-6 flex items-center">
                                <svg class="h-5 w-5" x-description="Expand icon, show/hide based on section open state. Heroicon name: solid/plus-sm" x-show="!(open)" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path fill-rule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clip-rule="evenodd"></path> </svg>
                                <svg class="h-5 w-5" x-description="Collapse icon, show/hide based on section open state. Heroicon name: solid/minus-sm" x-show="open" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true" style="display: none;"> <path fill-rule="evenodd" d="M5 10a1 1 0 011-1h8a1 1 0 110 2H6a1 1 0 01-1-1z" clip-rule="evenodd"></path> </svg>
                            </span>
                        </button>
                    </h3>
                    <div class="pt-6 px-4" x-show="open" style="display: none;">
                        <div class="space-y-4">

                            
                            <div class="flex items-center">
                                <input wire:model.defer="rating" id="filter-rating-5" name="rating" value="5" type="radio" class="h-4 w-4 border-gray-300 rounded-full text-primary-600 focus:ring-primary-600">
                                <label for="filter-rating-5" class="ltr:ml-3 rtl:mr-3 text-sm text-gray-600">
                                    <div class="flex items-center">
                                        <?php for($i = 0; $i < 5; $i++): ?>
                                            <svg class="text-amber-400 flex-shrink-0 h-5 w-5" x-description="Heroicon name: solid/star" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path> </svg>
                                        <?php endfor; ?>
                                    </div>
                                </label>
                            </div>

                            
                            <div class="flex items-center">
                                <input wire:model.defer="rating" id="filter-rating-4" name="rating" value="4" type="radio" class="h-4 w-4 border-gray-300 rounded-full text-primary-600 focus:ring-primary-600">
                                <label for="filter-rating-4" class="ltr:ml-3 rtl:mr-3 text-sm text-gray-600">
                                    <div class="flex items-center">
                                        <?php for($i = 0; $i < 4; $i++): ?>
                                            <svg class="text-amber-400 flex-shrink-0 h-5 w-5" x-description="Heroicon name: solid/star" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path> </svg>
                                        <?php endfor; ?>
                                        <svg class="text-gray-300 flex-shrink-0 h-5 w-5" x-description="Heroicon name: solid/star" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path> </svg>
                                    </div>
                                </label>
                            </div>

                            
                            <div class="flex items-center">
                                <input wire:model.defer="rating" id="filter-rating-3" name="rating" value="3" type="radio" class="h-4 w-4 border-gray-300 rounded-full text-primary-600 focus:ring-primary-600">
                                <label for="filter-rating-3" class="ltr:ml-3 rtl:mr-3 text-sm text-gray-600">
                                    <div class="flex items-center">
                                        <?php for($i = 0; $i < 3; $i++): ?>
                                            <svg class="text-amber-400 flex-shrink-0 h-5 w-5" x-description="Heroicon name: solid/star" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path> </svg>
                                        <?php endfor; ?>
                                        <?php for($i = 0; $i < 2; $i++): ?>
                                            <svg class="text-gray-300 flex-shrink-0 h-5 w-5" x-description="Heroicon name: solid/star" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path> </svg>
                                        <?php endfor; ?>
                                    </div>
                                </label>
                            </div>

                            
                            <div class="flex items-center">
                                <input wire:model.defer="rating" id="filter-rating-2" name="rating" value="2" type="radio" class="h-4 w-4 border-gray-300 rounded-full text-primary-600 focus:ring-primary-600">
                                <label for="filter-rating-2" class="ltr:ml-3 rtl:mr-3 text-sm text-gray-600">
                                    <div class="flex items-center">
                                        <?php for($i = 0; $i < 2; $i++): ?>
                                            <svg class="text-amber-400 flex-shrink-0 h-5 w-5" x-description="Heroicon name: solid/star" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path> </svg>
                                        <?php endfor; ?>
                                        <?php for($i = 0; $i < 3; $i++): ?>
                                            <svg class="text-gray-300 flex-shrink-0 h-5 w-5" x-description="Heroicon name: solid/star" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path> </svg>
                                        <?php endfor; ?>
                                    </div>
                                </label>
                            </div>

                            
                            <div class="flex items-center">
                                <input wire:model.defer="rating" id="filter-rating-1" name="rating" value="1" type="radio" class="h-4 w-4 border-gray-300 rounded-full text-primary-600 focus:ring-primary-600">
                                <label for="filter-rating-1" class="ltr:ml-3 rtl:mr-3 text-sm text-gray-600">
                                    <div class="flex items-center">
                                        <?php for($i = 0; $i < 1; $i++): ?>
                                            <svg class="text-amber-400 flex-shrink-0 h-5 w-5" x-description="Heroicon name: solid/star" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path> </svg>
                                        <?php endfor; ?>
                                        <?php for($i = 0; $i < 4; $i++): ?>
                                            <svg class="text-gray-300 flex-shrink-0 h-5 w-5" x-description="Heroicon name: solid/star" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path> </svg>
                                        <?php endfor; ?>
                                    </div>
                                </label>
                            </div>
                        
                        </div>
                    </div>
                </div>

                
                <div x-data="{ open: true }" class="py-3">
                    <h3 class="-my-3 flow-root bg-gray-50 dark:bg-zinc-700 px-4">
                        <button @click="open = !open" type="button" class="py-3 w-full flex items-center justify-between text-sm text-gray-400 hover:text-gray-500 outline-none focus:outline-none">
                            <span class="font-medium text-gray-900"><?php echo e(__('messages.t_price')); ?></span>
                            <span class="ltr:ml-6 rtl:mr-6 flex items-center">
                                <svg class="h-5 w-5" x-description="Expand icon, show/hide based on section open state. Heroicon name: solid/plus-sm" x-show="!(open)" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path fill-rule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clip-rule="evenodd"></path> </svg>
                                <svg class="h-5 w-5" x-description="Collapse icon, show/hide based on section open state. Heroicon name: solid/minus-sm" x-show="open" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true" style="display: none;"> <path fill-rule="evenodd" d="M5 10a1 1 0 011-1h8a1 1 0 110 2H6a1 1 0 01-1-1z" clip-rule="evenodd"></path> </svg>
                            </span>
                        </button>
                    </h3>
                    <div class="pt-6 px-4" x-show="open" style="display: none;">
                        <div class="space-y-4">

                            <div class="rounded-md shadow-sm -space-y-px">
                                <div>
                                    <input type="integer" wire:model.defer="min_price" minlength="1" min="1" maxlength="999999999" max="999999999" class="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 dark:placeholder-gray-200 dark:text-gray-100 text-gray-900 rounded-t-md focus:outline-none focus:ring-primary-600 focus:border-primary-600 focus:z-10 sm:text-sm font-medium dark:bg-transparent dark:border-zinc-600" placeholder="<?php echo e(__('messages.t_min_price')); ?>">
                                </div>
                                <div>
                                    <input type="integer" wire:model.defer="max_price" minlength="1" min="1" maxlength="999999999" max="999999999" required="" class="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 dark:placeholder-gray-200 dark:text-gray-100 text-gray-900 rounded-b-md focus:outline-none focus:ring-primary-600 focus:border-primary-600 focus:z-10 sm:text-sm font-medium dark:bg-transparent dark:border-zinc-600" placeholder="<?php echo e(__('messages.t_max_price')); ?>">
                                </div>
                            </div>
                        
                        </div>
                    </div>
                </div>

                
                <div x-data="{ open: true }" class="py-3">
                    <h3 class="-my-3 flow-root bg-gray-50 dark:bg-zinc-700 px-4">
                        <button @click="open = !open" type="button" class="py-3 w-full flex items-center justify-between text-sm text-gray-400 hover:text-gray-500 outline-none focus:outline-none">
                            <span class="font-medium text-gray-900"><?php echo e(__('messages.t_delivery_time')); ?></span>
                            <span class="ltr:ml-6 rtl:mr-6 flex items-center">
                                <svg class="h-5 w-5" x-description="Expand icon, show/hide based on section open state. Heroicon name: solid/plus-sm" x-show="!(open)" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path fill-rule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clip-rule="evenodd"></path> </svg>
                                <svg class="h-5 w-5" x-description="Collapse icon, show/hide based on section open state. Heroicon name: solid/minus-sm" x-show="open" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true" style="display: none;"> <path fill-rule="evenodd" d="M5 10a1 1 0 011-1h8a1 1 0 110 2H6a1 1 0 01-1-1z" clip-rule="evenodd"></path> </svg>
                            </span>
                        </button>
                    </h3>
                    <div class="pt-6 px-4" x-show="open" style="display: none;">
                        <div class="space-y-4">

                            <?php $__currentLoopData = $delivery_times; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $time): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="flex items-center">
                                    <input wire:model.defer="delivery_time" id="filter-delivery-time-<?php echo e($key); ?>" value="<?php echo e($time['value']); ?>" name="delivery_time" type="radio" class="focus:ring-primary-600 h-4 w-4 text-primary-600 border-gray-300">
                                    <label for="filter-delivery-time-<?php echo e($key); ?>" class="ltr:ml-3 rtl:mr-3 block text-sm font-medium text-gray-700 dark:text-gray-300">
                                        <?php echo e($time['text']); ?>

                                    </label>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        
                        </div>
                    </div>
                </div>

                
                <div class="py-6 px-4">

                    
                    <?php if (isset($component)) { $__componentOriginal49b2fc8ba42c39d638e648b21b88e1b33ae2822c = $component; } ?>
<?php $component = App\View\Components\Forms\Button::resolve([] + (isset($attributes) && $attributes instanceof Illuminate\View\ComponentAttributeBag ? (array) $attributes->getIterator() : [])); ?>
<?php $component->withName('forms.button'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php if (isset($attributes) && $attributes instanceof Illuminate\View\ComponentAttributeBag && $constructor = (new ReflectionClass(App\View\Components\Forms\Button::class))->getConstructor()): ?>
<?php $attributes = $attributes->except(collect($constructor->getParameters())->map->getName()->all()); ?>
<?php endif; ?>
<?php $component->withAttributes(['action' => 'filter','text' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute(__('messages.t_filter')),'block' => true]); ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?>
<?php if (isset($__componentOriginal49b2fc8ba42c39d638e648b21b88e1b33ae2822c)): ?>
<?php $component = $__componentOriginal49b2fc8ba42c39d638e648b21b88e1b33ae2822c; ?>
<?php unset($__componentOriginal49b2fc8ba42c39d638e648b21b88e1b33ae2822c); ?>
<?php endif; ?>

                    
                    
                    <?php if($rating || $delivery_time || $min_price || $max_price): ?>
                        <span wire:click="resetFilter" class="hover:underline text-xs font-medium text-gray-600 hover:text-gray-800 mt-4 text-center block cursor-pointer"><?php echo e(__('messages.t_reset_filter')); ?></span>
                        
                    <?php endif; ?>
                    
                </div>

            </div>

        </div>
    
    </div>
    
    
    <main class="px-4 sm:px-6 lg:px-8">

        
        <div class="flex justify-between items-center mb-2 bg-transparent py-2 ltr:pr-6 rtl:pl-6 ltr:border-l-8 rtl:border-r-8 ltr:pl-4 rtl:pr-4 border-primary-600 rounded">

            
            <div>
                <span class="font-extrabold text-base text-gray-800 dark:text-gray-100 pb-1 block tracking-wider"><?php echo e($q); ?></span>
                <p class="text-sm text-gray-400"><?php echo e(__('messages.t_search_results_for_q', ['q' => $q])); ?></p>
            </div>

            
            <div>
                <div class="flex items-center">

                    
                    <div x-data="Components.menu({ open: false })" x-init="init()" @keydown.escape.stop="open = false; focusButton()" @click.away="onClickAway($event)" class="relative inline-block ltr:text-left rtl:text-right">

                        
                        <div>
                            <button type="button" class="group inline-flex justify-center text-sm font-medium text-gray-700 dark:text-gray-300 hover:text-gray-900 dark:hover:text-gray-200" id="menu-button" x-ref="button" @click="onButtonClick()" @keyup.space.prevent="onButtonEnter()" @keydown.enter.prevent="onButtonEnter()" aria-expanded="false" aria-haspopup="true" x-bind:aria-expanded="open.toString()" @keydown.arrow-up.prevent="onArrowUp()" @keydown.arrow-down.prevent="onArrowDown()">
                                <?php echo e(__('messages.t_sort_by')); ?>

                                <svg class="flex-shrink-0 ltr:-mr-1 rtl:-ml-1 ltr:ml-1 rtl:mr-1 h-5 w-5 text-gray-400 dark:text-gray-300 group-hover:text-gray-500 dark:group-hover:text-gray-200 dark:hover:text-gray-200" x-description="Heroicon name: solid/chevron-down" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path> </svg>
                            </button>
                        </div>
        
                        
                        <div x-show="open" x-transition:enter="transition ease-out duration-100" x-transition:enter-start="transform opacity-0 scale-95" x-transition:enter-end="transform opacity-100 scale-100" x-transition:leave="transition ease-in duration-75" x-transition:leave-start="transform opacity-100 scale-100" x-transition:leave-end="transform opacity-0 scale-95" class="ltr:origin-top-right rtl:origin-top-left absolute ltr:right-0 rtl:left-0 mt-2 w-40 rounded-md shadow-sm bg-white dark:bg-zinc-800 ring-1 ring-black ring-opacity-5 focus:outline-none z-50" x-ref="menu-items" x-bind:aria-activedescendant="activeDescendant" role="menu" aria-orientation="vertical" tabindex="-1" @keydown.arrow-up.prevent="onArrowUp()" @keydown.arrow-down.prevent="onArrowDown()" @keydown.tab="open = false" @keydown.enter.prevent="open = false; focusButton()" @keyup.space.prevent="open = false; focusButton()" style="display: none;">
                            <div class="py-1" role="none">
                            
                                
                                <button wire:click="$set('sort_by', 'popular')" type="button" class="<?php echo e($sort_by === 'popular' ? 'text-gray-900' : 'text-gray-500'); ?> block px-4 py-3 text-xs font-medium hover:bg-gray-100 dark:hover:bg-zinc-700 dark:text-gray-400 w-full ltr:text-left rtl:text-right">
                                    <?php echo e(__('messages.t_most_popular')); ?>

                                </button>

                                
                                <button wire:click="$set('sort_by', 'rating')" type="button" class="<?php echo e($sort_by === 'rating' ? 'text-gray-900' : 'text-gray-500'); ?> block px-4 py-3 text-xs font-medium hover:bg-gray-100 dark:hover:bg-zinc-700 dark:text-gray-400 w-full ltr:text-left rtl:text-right">
                                    <?php echo e(__('messages.t_best_rating')); ?>

                                </button>

                                
                                <button wire:click="$set('sort_by', 'sales')" type="button" class="<?php echo e($sort_by === 'sales' ? 'text-gray-900' : 'text-gray-500'); ?> block px-4 py-3 text-xs font-medium hover:bg-gray-100 dark:hover:bg-zinc-700 dark:text-gray-400 w-full ltr:text-left rtl:text-right">
                                    <?php echo e(__('messages.t_most_selling')); ?>

                                </button>

                                
                                <button wire:click="$set('sort_by', 'newest')" type="button" class="<?php echo e($sort_by === 'newest' ? 'text-gray-900' : 'text-gray-500'); ?> block px-4 py-3 text-xs font-medium hover:bg-gray-100 dark:hover:bg-zinc-700 dark:text-gray-400 w-full ltr:text-left rtl:text-right">
                                    <?php echo e(__('messages.t_newest_first')); ?>

                                </button>

                                
                                <button wire:click="$set('sort_by', 'price_low_high')" type="button" class="<?php echo e($sort_by === 'price_low_high' ? 'text-gray-900' : 'text-gray-500'); ?> block px-4 py-3 text-xs font-medium hover:bg-gray-100 dark:hover:bg-zinc-700 dark:text-gray-400 w-full ltr:text-left rtl:text-right">
                                    <?php echo e(__('messages.t_price_low_to_high')); ?>

                                </button>

                                
                                <button wire:click="$set('sort_by', 'price_high_low')" type="button" class="<?php echo e($sort_by === 'price_high_low' ? 'text-gray-900' : 'text-gray-500'); ?> block px-4 py-3 text-xs font-medium hover:bg-gray-100 dark:hover:bg-zinc-700 dark:text-gray-400 w-full ltr:text-left rtl:text-right">
                                    <?php echo e(__('messages.t_price_high_to_low')); ?>

                                </button>
                            
                            </div>
                        </div>
                    
                    </div>
        
                    
                    <button type="button" class="p-2 -m-2 ltr:ml-4 rtl:mr-4 ltr:sm:ml-6 rtl:sm:mr-6 text-gray-400 hover:text-gray-500 lg:hidden" @click="open = true">
                        <svg class="w-4 h-4" aria-hidden="true" x-description="Heroicon name: solid/filter" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor"> <path fill-rule="evenodd" d="M3 3a1 1 0 011-1h12a1 1 0 011 1v3a1 1 0 01-.293.707L12 11.414V15a1 1 0 01-.293.707l-2 2A1 1 0 018 17v-5.586L3.293 6.707A1 1 0 013 6V3z" clip-rule="evenodd"></path> </svg>
                    </button>

                </div>
            </div>

        </div>

        
        <section aria-labelledby="products-heading" class="pt-6 pb-24">
            <div class="grid grid-cols-1 lg:grid-cols-3 xl:grid-cols-4 gap-x-6 gap-y-10">

                
                <div>

                    
                    <div class="hidden lg:block bg-white dark:bg-zinc-700 shadow-sm border rounded-md border-gray-100 dark:border-zinc-600 h-fit">
                        
                        
                        <div x-data="{ open: true }" class="py-3">
                            <h3 class="-my-3 flow-root bg-gray-50 dark:bg-zinc-600 px-4 rounded-t-md">
                                <button @click="open = !open" type="button" class="py-3 w-full flex items-center justify-between text-sm text-gray-400 hover:text-gray-500 outline-none focus:outline-none">
                                    <span class="font-medium text-gray-900 dark:text-gray-300"><?php echo e(__('messages.t_rating')); ?></span>
                                    <span class="ltr:ml-6 rtl:mr-6 flex items-center">
                                        <svg class="h-5 w-5" x-description="Expand icon, show/hide based on section open state. Heroicon name: solid/plus-sm" x-show="!(open)" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path fill-rule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clip-rule="evenodd"></path> </svg>
                                        <svg class="h-5 w-5" x-description="Collapse icon, show/hide based on section open state. Heroicon name: solid/minus-sm" x-show="open" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true" style="display: none;"> <path fill-rule="evenodd" d="M5 10a1 1 0 011-1h8a1 1 0 110 2H6a1 1 0 01-1-1z" clip-rule="evenodd"></path> </svg>
                                    </span>
                                </button>
                            </h3>
                            <div class="pt-6 px-4" x-show="open" style="display: none;">
                                <div class="space-y-4">
    
                                    
                                    <div class="flex items-center">
                                        <input wire:model.defer="rating" id="filter-rating-5" name="rating" value="5" type="radio" class="h-4 w-4 border-gray-300 rounded-full text-primary-600 focus:ring-primary-600">
                                        <label for="filter-rating-5" class="ltr:ml-3 rtl:mr-3 text-sm text-gray-600">
                                            <div class="flex items-center">
                                                <?php for($i = 0; $i < 5; $i++): ?>
                                                    <svg class="text-amber-400 flex-shrink-0 h-5 w-5" x-description="Heroicon name: solid/star" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path> </svg>
                                                <?php endfor; ?>
                                            </div>
                                        </label>
                                    </div>
    
                                    
                                    <div class="flex items-center">
                                        <input wire:model.defer="rating" id="filter-rating-4" name="rating" value="4" type="radio" class="h-4 w-4 border-gray-300 rounded-full text-primary-600 focus:ring-primary-600">
                                        <label for="filter-rating-4" class="ltr:ml-3 rtl:mr-3 text-sm text-gray-600">
                                            <div class="flex items-center">
                                                <?php for($i = 0; $i < 4; $i++): ?>
                                                    <svg class="text-amber-400 flex-shrink-0 h-5 w-5" x-description="Heroicon name: solid/star" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path> </svg>
                                                <?php endfor; ?>
                                                <svg class="text-gray-300 flex-shrink-0 h-5 w-5" x-description="Heroicon name: solid/star" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path> </svg>
                                            </div>
                                        </label>
                                    </div>
    
                                    
                                    <div class="flex items-center">
                                        <input wire:model.defer="rating" id="filter-rating-3" name="rating" value="3" type="radio" class="h-4 w-4 border-gray-300 rounded-full text-primary-600 focus:ring-primary-600">
                                        <label for="filter-rating-3" class="ltr:ml-3 rtl:mr-3 text-sm text-gray-600">
                                            <div class="flex items-center">
                                                <?php for($i = 0; $i < 3; $i++): ?>
                                                    <svg class="text-amber-400 flex-shrink-0 h-5 w-5" x-description="Heroicon name: solid/star" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path> </svg>
                                                <?php endfor; ?>
                                                <?php for($i = 0; $i < 2; $i++): ?>
                                                    <svg class="text-gray-300 flex-shrink-0 h-5 w-5" x-description="Heroicon name: solid/star" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path> </svg>
                                                <?php endfor; ?>
                                            </div>
                                        </label>
                                    </div>
    
                                    
                                    <div class="flex items-center">
                                        <input wire:model.defer="rating" id="filter-rating-2" name="rating" value="2" type="radio" class="h-4 w-4 border-gray-300 rounded-full text-primary-600 focus:ring-primary-600">
                                        <label for="filter-rating-2" class="ltr:ml-3 rtl:mr-3 text-sm text-gray-600">
                                            <div class="flex items-center">
                                                <?php for($i = 0; $i < 2; $i++): ?>
                                                    <svg class="text-amber-400 flex-shrink-0 h-5 w-5" x-description="Heroicon name: solid/star" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path> </svg>
                                                <?php endfor; ?>
                                                <?php for($i = 0; $i < 3; $i++): ?>
                                                    <svg class="text-gray-300 flex-shrink-0 h-5 w-5" x-description="Heroicon name: solid/star" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path> </svg>
                                                <?php endfor; ?>
                                            </div>
                                        </label>
                                    </div>
    
                                    
                                    <div class="flex items-center">
                                        <input wire:model.defer="rating" id="filter-rating-1" name="rating" value="1" type="radio" class="h-4 w-4 border-gray-300 rounded-full text-primary-600 focus:ring-primary-600">
                                        <label for="filter-rating-1" class="ltr:ml-3 rtl:mr-3 text-sm text-gray-600">
                                            <div class="flex items-center">
                                                <?php for($i = 0; $i < 1; $i++): ?>
                                                    <svg class="text-amber-400 flex-shrink-0 h-5 w-5" x-description="Heroicon name: solid/star" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path> </svg>
                                                <?php endfor; ?>
                                                <?php for($i = 0; $i < 4; $i++): ?>
                                                    <svg class="text-gray-300 flex-shrink-0 h-5 w-5" x-description="Heroicon name: solid/star" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path> </svg>
                                                <?php endfor; ?>
                                            </div>
                                        </label>
                                    </div>
                                
                                </div>
                            </div>
                        </div>
    
                        
                        <div x-data="{ open: true }" class="py-3">
                            <h3 class="-my-3 flow-root bg-gray-50 dark:bg-zinc-600 px-4">
                                <button @click="open = !open" type="button" class="py-3 w-full flex items-center justify-between text-sm text-gray-400 hover:text-gray-500 outline-none focus:outline-none">
                                    <span class="font-medium text-gray-900 dark:text-gray-300"><?php echo e(__('messages.t_price')); ?></span>
                                    <span class="ltr:ml-6 rtl:mr-6 flex items-center">
                                        <svg class="h-5 w-5" x-description="Expand icon, show/hide based on section open state. Heroicon name: solid/plus-sm" x-show="!(open)" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path fill-rule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clip-rule="evenodd"></path> </svg>
                                        <svg class="h-5 w-5" x-description="Collapse icon, show/hide based on section open state. Heroicon name: solid/minus-sm" x-show="open" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true" style="display: none;"> <path fill-rule="evenodd" d="M5 10a1 1 0 011-1h8a1 1 0 110 2H6a1 1 0 01-1-1z" clip-rule="evenodd"></path> </svg>
                                    </span>
                                </button>
                            </h3>
                            <div class="pt-6 px-4" x-show="open" style="display: none;">
                                <div class="space-y-4">
    
                                    <div class="rounded-md shadow-sm -space-y-px">
                                        <div>
                                            <input type="integer" wire:model.defer="min_price" minlength="1" min="1" maxlength="999999999" max="999999999" class="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 dark:placeholder-gray-200 dark:text-gray-100 text-gray-900 rounded-t-md focus:outline-none focus:ring-primary-600 focus:border-primary-600 focus:z-10 sm:text-sm font-medium dark:bg-transparent dark:border-zinc-600" placeholder="<?php echo e(__('messages.t_min_price')); ?>">
                                        </div>
                                        <div>
                                            <input type="integer" wire:model.defer="max_price" minlength="1" min="1" maxlength="999999999" max="999999999" required="" class="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 dark:placeholder-gray-200 dark:text-gray-100 text-gray-900 rounded-b-md focus:outline-none focus:ring-primary-600 focus:border-primary-600 focus:z-10 sm:text-sm font-medium dark:bg-transparent dark:border-zinc-600" placeholder="<?php echo e(__('messages.t_max_price')); ?>">
                                        </div>
                                    </div>
                                
                                </div>
                            </div>
                        </div>
    
                        
                        <div x-data="{ open: true }" class="py-3">
                            <h3 class="-my-3 flow-root bg-gray-50 dark:bg-zinc-600 px-4">
                                <button @click="open = !open" type="button" class="py-3 w-full flex items-center justify-between text-sm text-gray-400 hover:text-gray-500 outline-none focus:outline-none">
                                    <span class="font-medium text-gray-900 dark:text-gray-300"><?php echo e(__('messages.t_delivery_time')); ?></span>
                                    <span class="ltr:ml-6 rtl:mr-6 flex items-center">
                                        <svg class="h-5 w-5" x-description="Expand icon, show/hide based on section open state. Heroicon name: solid/plus-sm" x-show="!(open)" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"> <path fill-rule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clip-rule="evenodd"></path> </svg>
                                        <svg class="h-5 w-5" x-description="Collapse icon, show/hide based on section open state. Heroicon name: solid/minus-sm" x-show="open" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true" style="display: none;"> <path fill-rule="evenodd" d="M5 10a1 1 0 011-1h8a1 1 0 110 2H6a1 1 0 01-1-1z" clip-rule="evenodd"></path> </svg>
                                    </span>
                                </button>
                            </h3>
                            <div class="pt-6 px-4" x-show="open" style="display: none;">
                                <div class="space-y-4">
    
                                    <?php $__currentLoopData = $delivery_times; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $time): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="flex items-center">
                                            <input wire:model.defer="delivery_time" id="filter-delivery-time-<?php echo e($key); ?>" value="<?php echo e($time['value']); ?>" name="delivery_time" type="radio" class="focus:ring-primary-600 h-4 w-4 text-primary-600 border-gray-300">
                                            <label for="filter-delivery-time-<?php echo e($key); ?>" class="ltr:ml-3 rtl:mr-3 block text-sm font-medium text-gray-700 dark:text-gray-300">
                                                <?php echo e($time['text']); ?>

                                            </label>
                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                
                                </div>
                            </div>
                        </div>
    
                        
                        <div class="py-6 px-4">
    
                            
                            <?php if (isset($component)) { $__componentOriginal49b2fc8ba42c39d638e648b21b88e1b33ae2822c = $component; } ?>
<?php $component = App\View\Components\Forms\Button::resolve([] + (isset($attributes) && $attributes instanceof Illuminate\View\ComponentAttributeBag ? (array) $attributes->getIterator() : [])); ?>
<?php $component->withName('forms.button'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php if (isset($attributes) && $attributes instanceof Illuminate\View\ComponentAttributeBag && $constructor = (new ReflectionClass(App\View\Components\Forms\Button::class))->getConstructor()): ?>
<?php $attributes = $attributes->except(collect($constructor->getParameters())->map->getName()->all()); ?>
<?php endif; ?>
<?php $component->withAttributes(['action' => 'filter','text' => \Illuminate\View\Compilers\BladeCompiler::sanitizeComponentAttribute(__('messages.t_filter')),'block' => true]); ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?>
<?php if (isset($__componentOriginal49b2fc8ba42c39d638e648b21b88e1b33ae2822c)): ?>
<?php $component = $__componentOriginal49b2fc8ba42c39d638e648b21b88e1b33ae2822c; ?>
<?php unset($__componentOriginal49b2fc8ba42c39d638e648b21b88e1b33ae2822c); ?>
<?php endif; ?>
    
                            
                            <?php if($rating || $delivery_time || $min_price || $max_price): ?>
                                <span wire:click="resetFilter" class="hover:underline text-xs font-medium text-gray-600 hover:text-gray-800 mt-4 text-center block cursor-pointer"><?php echo e(__('messages.t_reset_filter')); ?></span>
                            <?php endif; ?>
    
                        </div>
                        
                    </div>

                </div>

                
                 <div class="lg:col-span-2 xl:col-span-3">
                                <div class="grid grid-cols-12 sm:gap-x-6 gap-y-6">



                                    <div class="col-span-12 lg:col-span-6 xl:col-span-4 md:col-span-6 sm:col-span-6"
                                        wire:key="gigs-list-01F847B3844BFEEE21B6">
                                        <div wire:id="JYoAWdZmVf7NmfCHv7Wr"
                                            wire:initial-data="{&quot;fingerprint&quot;:{&quot;id&quot;:&quot;JYoAWdZmVf7NmfCHv7Wr&quot;,&quot;name&quot;:&quot;main.cards.gig&quot;,&quot;locale&quot;:&quot;en&quot;,&quot;path&quot;:&quot;categories\/music-audio\/voice-over-streaming&quot;,&quot;method&quot;:&quot;GET&quot;,&quot;v&quot;:&quot;acj&quot;},&quot;effects&quot;:{&quot;listeners&quot;:[]},&quot;serverMemo&quot;:{&quot;children&quot;:[],&quot;errors&quot;:[],&quot;htmlHash&quot;:&quot;a30c8103&quot;,&quot;data&quot;:{&quot;gig&quot;:[],&quot;favorite&quot;:false,&quot;profile_visible&quot;:true},&quot;dataMeta&quot;:{&quot;models&quot;:{&quot;gig&quot;:{&quot;class&quot;:&quot;App\\Models\\Gig&quot;,&quot;id&quot;:214,&quot;relations&quot;:[&quot;thumbnail&quot;,&quot;owner&quot;,&quot;owner.avatar&quot;],&quot;connection&quot;:&quot;mysql&quot;,&quot;collectionClass&quot;:null}}},&quot;checksum&quot;:&quot;686edb6038dc7c1f52d948008086146bff0f3e06e597d0dc475f3138639bfbcd&quot;}}"
                                            class="gig-card" x-data="window._01F847B3844BFEEE21B6" dir="ltr">
                                            <div
                                                class="bg-white dark:bg-zinc-800 rounded-md shadow-sm ring-1 ring-gray-200 dark:ring-zinc-800">


                                                <a href="<?php echo e(url('/post-work-description')); ?>"
                                                    class="flex items-center justify-center overflow-hidden w-full h-52 bg-gray-100 dark:bg-zinc-700">
                                                    <img class="object-contain max-h-52 lazy h-52 w-auto" width="200"
                                                        src="../../public/storage/site/placeholder/3A094BE8F9E5093D0205.webp"
                                                        data-src="https://riverr.edhub.me/public/storage/gigs/previews/small/27F84E7AC160A59D6EC6.jpg"
                                                        alt="Record a pro american voiceover for your company">
                                                </a>


                                                <div class="px-4 pb-2 mt-4">


                                                    <div class="w-full mb-4 flex justify-between items-center">
                                                        <a href="../../profile/EdwardHendrix.html" target="_blank"
                                                            class="flex-shrink-0 group block">
                                                            <div class="flex items-center">
                                                                <span class="inline-block relative">
                                                                    <img class="h-6 w-6 rounded-full object-cover lazy"
                                                                        src="../../public/storage/site/placeholder/3A094BE8F9E5093D0205.webp"
                                                                        data-src="https://riverr.edhub.me/public/storage/avatars/BEF099D3385E2245EA89.png"
                                                                        alt="EdwardHendrix">
                                                                </span>
                                                                <div class="ltr:ml-3 rtl:mr-3">
                                                                    <div
                                                                        class="text-gray-500 dark:text-gray-200 hover:text-gray-900 dark:hover:text-gray-300 flex items-center mb-0.5 font-extrabold tracking-wide text-[13px]">
                                                                        EdwardHendrix
                                                                        <img data-tooltip-target="tooltip-account-verified-01F847B3844BFEEE21B6"
                                                                            class="ltr:ml-0.5 rtl:mr-0.5 h-4 w-4 -mt-0.5"
                                                                            src="../../public/img/auth/verified-badge.svg"
                                                                            alt="Account verified">
                                                                        <div id="tooltip-account-verified-01F847B3844BFEEE21B6"
                                                                            role="tooltip"
                                                                            class="inline-block absolute invisible z-10 py-2 px-3 text-xs font-medium text-white bg-gray-900 rounded-sm shadow-sm opacity-0 tooltip dark:bg-gray-700">
                                                                            Account verified
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>


                                                    <a href="../../service/record-a-pro-american-voiceover-for-your-company-01F847B3844BFEEE21B6.html"
                                                        class="gig-card-title font-semibold text-gray-800 dark:text-gray-100 hover:text-primary-600 dark:hover:text-white mb-4 !overflow-hidden">
                                                        Record a pro american voiceover for your company
                                                    </a>


                                                    <div class="flex items-center" wire:ignore>


                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                            class="h-5 w-5 text-amber-400" viewBox="0 0 20 20"
                                                            fill="currentColor">
                                                            <path
                                                                d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                                                        </svg>


                                                        <div
                                                            class=" text-[13px] tracking-widest text-amber-500 ltr:ml-1 rtl:mr-1 font-black">
                                                            N/A</div>


                                                        <div
                                                            class="ltr:ml-2 rtl:mr-2 text-[13px] font-normal text-gray-400">
                                                            ( 0 )
                                                        </div>

                                                    </div>

                                                </div>


                                                <div
                                                    class="px-3 py-3 bg-[#fdfdfd] dark:bg-zinc-800 border-t border-gray-50 dark:border-zinc-700 text-right sm:px-4 rounded-b-md flex justify-between">

                                                    <div class="flex items-center">


                                                        <button x-on:click="loginToAddToFavorite"
                                                            data-tooltip-target="tooltip-add-to-favorites-01F847B3844BFEEE21B6"
                                                            class="h-8 w-8 rounded-full flex items-center justify-center -ml-1 focus:outline-none visited:outline-none">




                                                            <svg class="w-5 h-5 text-gray-400 hover:text-gray-500"
                                                                stroke="currentColor" fill="currentColor"
                                                                stroke-width="0" viewBox="0 0 20 20"
                                                                xmlns="http://www.w3.org/2000/svg">
                                                                <path fill-rule="evenodd"
                                                                    d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z"
                                                                    clip-rule="evenodd"></path>
                                                            </svg>

                                                        </button>
                                                        <div id="tooltip-add-to-favorites-01F847B3844BFEEE21B6"
                                                            role="tooltip"
                                                            class="inline-block absolute invisible z-10 py-2 px-3 text-xs font-medium text-white bg-gray-900 rounded-sm shadow-sm opacity-0 tooltip dark:bg-gray-700">
                                                            Add to favorite
                                                        </div>

                                                    </div>


                                                    <div class="gig-card-price">
                                                        <small class="text-body-3 dark:!text-zinc-200">Starting
                                                            at</small>
                                                        <span
                                                            class=" ltr:text-right rtl:text-left dark:!text-white">$6.99</span>
                                                    </div>

                                                </div>

                                            </div>

                                        </div>


                                        <!-- Livewire Component wire-end:JYoAWdZmVf7NmfCHv7Wr -->
                                    </div>



                                    <div class="col-span-12 lg:col-span-6 xl:col-span-4 md:col-span-6 sm:col-span-6"
                                        wire:key="gigs-list-425CED466DB20C5800F2">
                                        <div wire:id="21tJX9EjsW5ywkY53mEE"
                                            wire:initial-data="{&quot;fingerprint&quot;:{&quot;id&quot;:&quot;21tJX9EjsW5ywkY53mEE&quot;,&quot;name&quot;:&quot;main.cards.gig&quot;,&quot;locale&quot;:&quot;en&quot;,&quot;path&quot;:&quot;categories\/music-audio\/voice-over-streaming&quot;,&quot;method&quot;:&quot;GET&quot;,&quot;v&quot;:&quot;acj&quot;},&quot;effects&quot;:{&quot;listeners&quot;:[]},&quot;serverMemo&quot;:{&quot;children&quot;:[],&quot;errors&quot;:[],&quot;htmlHash&quot;:&quot;cd8f46ff&quot;,&quot;data&quot;:{&quot;gig&quot;:[],&quot;favorite&quot;:false,&quot;profile_visible&quot;:true},&quot;dataMeta&quot;:{&quot;models&quot;:{&quot;gig&quot;:{&quot;class&quot;:&quot;App\\Models\\Gig&quot;,&quot;id&quot;:215,&quot;relations&quot;:[&quot;thumbnail&quot;,&quot;owner&quot;,&quot;owner.avatar&quot;],&quot;connection&quot;:&quot;mysql&quot;,&quot;collectionClass&quot;:null}}},&quot;checksum&quot;:&quot;c777148891001530bacd15d2636b9640e0ddb6688bccd7cfc1ef1a21b35ddee0&quot;}}"
                                            class="gig-card" x-data="window._425CED466DB20C5800F2" dir="ltr">
                                            <div
                                                class="bg-white dark:bg-zinc-800 rounded-md shadow-sm ring-1 ring-gray-200 dark:ring-zinc-800">


                                                <a href="../../service/record-an-american-male-voice-over-425CED466DB20C5800F2.html"
                                                    class="flex items-center justify-center overflow-hidden w-full h-52 bg-gray-100 dark:bg-zinc-700">
                                                    <img class="object-contain max-h-52 lazy h-52 w-auto" width="200"
                                                        src="../../public/storage/site/placeholder/3A094BE8F9E5093D0205.webp"
                                                        data-src="https://riverr.edhub.me/public/storage/gigs/previews/small/37592D9311B47F51FADB.jpg"
                                                        alt="Record an american male voice over">
                                                </a>


                                                <div class="px-4 pb-2 mt-4">


                                                    <div class="w-full mb-4 flex justify-between items-center">
                                                        <a href="../../profile/EdwardHendrix.html" target="_blank"
                                                            class="flex-shrink-0 group block">
                                                            <div class="flex items-center">
                                                                <span class="inline-block relative">
                                                                    <img class="h-6 w-6 rounded-full object-cover lazy"
                                                                        src="../../public/storage/site/placeholder/3A094BE8F9E5093D0205.webp"
                                                                        data-src="https://riverr.edhub.me/public/storage/avatars/BEF099D3385E2245EA89.png"
                                                                        alt="EdwardHendrix">
                                                                </span>
                                                                <div class="ltr:ml-3 rtl:mr-3">
                                                                    <div
                                                                        class="text-gray-500 dark:text-gray-200 hover:text-gray-900 dark:hover:text-gray-300 flex items-center mb-0.5 font-extrabold tracking-wide text-[13px]">
                                                                        EdwardHendrix
                                                                        <img data-tooltip-target="tooltip-account-verified-425CED466DB20C5800F2"
                                                                            class="ltr:ml-0.5 rtl:mr-0.5 h-4 w-4 -mt-0.5"
                                                                            src="../../public/img/auth/verified-badge.svg"
                                                                            alt="Account verified">
                                                                        <div id="tooltip-account-verified-425CED466DB20C5800F2"
                                                                            role="tooltip"
                                                                            class="inline-block absolute invisible z-10 py-2 px-3 text-xs font-medium text-white bg-gray-900 rounded-sm shadow-sm opacity-0 tooltip dark:bg-gray-700">
                                                                            Account verified
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>


                                                    <a href="../../service/record-an-american-male-voice-over-425CED466DB20C5800F2.html"
                                                        class="gig-card-title font-semibold text-gray-800 dark:text-gray-100 hover:text-primary-600 dark:hover:text-white mb-4 !overflow-hidden">
                                                        Record an american male voice over
                                                    </a>


                                                    <div class="flex items-center" wire:ignore>


                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                            class="h-5 w-5 text-amber-400" viewBox="0 0 20 20"
                                                            fill="currentColor">
                                                            <path
                                                                d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                                                        </svg>


                                                        <div
                                                            class=" text-[13px] tracking-widest text-amber-500 ltr:ml-1 rtl:mr-1 font-black">
                                                            N/A</div>


                                                        <div
                                                            class="ltr:ml-2 rtl:mr-2 text-[13px] font-normal text-gray-400">
                                                            ( 0 )
                                                        </div>

                                                    </div>

                                                </div>


                                                <div
                                                    class="px-3 py-3 bg-[#fdfdfd] dark:bg-zinc-800 border-t border-gray-50 dark:border-zinc-700 text-right sm:px-4 rounded-b-md flex justify-between">

                                                    <div class="flex items-center">


                                                        <button x-on:click="loginToAddToFavorite"
                                                            data-tooltip-target="tooltip-add-to-favorites-425CED466DB20C5800F2"
                                                            class="h-8 w-8 rounded-full flex items-center justify-center -ml-1 focus:outline-none visited:outline-none">




                                                            <svg class="w-5 h-5 text-gray-400 hover:text-gray-500"
                                                                stroke="currentColor" fill="currentColor"
                                                                stroke-width="0" viewBox="0 0 20 20"
                                                                xmlns="http://www.w3.org/2000/svg">
                                                                <path fill-rule="evenodd"
                                                                    d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z"
                                                                    clip-rule="evenodd"></path>
                                                            </svg>

                                                        </button>
                                                        <div id="tooltip-add-to-favorites-425CED466DB20C5800F2"
                                                            role="tooltip"
                                                            class="inline-block absolute invisible z-10 py-2 px-3 text-xs font-medium text-white bg-gray-900 rounded-sm shadow-sm opacity-0 tooltip dark:bg-gray-700">
                                                            Add to favorite
                                                        </div>

                                                    </div>


                                                    <div class="gig-card-price">
                                                        <small class="text-body-3 dark:!text-zinc-200">Starting
                                                            at</small>
                                                        <span
                                                            class=" ltr:text-right rtl:text-left dark:!text-white">$12.00</span>
                                                    </div>

                                                </div>

                                            </div>

                                        </div>


                                        <!-- Livewire Component wire-end:21tJX9EjsW5ywkY53mEE -->
                                    </div>



                                    <div class="col-span-12 lg:col-span-6 xl:col-span-4 md:col-span-6 sm:col-span-6"
                                        wire:key="gigs-list-EF4353366FCEF0AB3B7E">
                                        <div wire:id="NdAtZUj0MroUDgseO9la"
                                            wire:initial-data="{&quot;fingerprint&quot;:{&quot;id&quot;:&quot;NdAtZUj0MroUDgseO9la&quot;,&quot;name&quot;:&quot;main.cards.gig&quot;,&quot;locale&quot;:&quot;en&quot;,&quot;path&quot;:&quot;categories\/music-audio\/voice-over-streaming&quot;,&quot;method&quot;:&quot;GET&quot;,&quot;v&quot;:&quot;acj&quot;},&quot;effects&quot;:{&quot;listeners&quot;:[]},&quot;serverMemo&quot;:{&quot;children&quot;:[],&quot;errors&quot;:[],&quot;htmlHash&quot;:&quot;9a1c70ee&quot;,&quot;data&quot;:{&quot;gig&quot;:[],&quot;favorite&quot;:false,&quot;profile_visible&quot;:true},&quot;dataMeta&quot;:{&quot;models&quot;:{&quot;gig&quot;:{&quot;class&quot;:&quot;App\\Models\\Gig&quot;,&quot;id&quot;:216,&quot;relations&quot;:[&quot;thumbnail&quot;,&quot;owner&quot;,&quot;owner.avatar&quot;],&quot;connection&quot;:&quot;mysql&quot;,&quot;collectionClass&quot;:null}}},&quot;checksum&quot;:&quot;837c501880219db6cf85c3dbba92831d5d34fd24ed2e67b5995dcd60cbcc2dd5&quot;}}"
                                            class="gig-card" x-data="window._EF4353366FCEF0AB3B7E" dir="ltr">
                                            <div
                                                class="bg-white dark:bg-zinc-800 rounded-md shadow-sm ring-1 ring-gray-200 dark:ring-zinc-800">


                                                <a href="../../service/professionally-edit-and-master-your-podcast-in-24-hours-EF4353366FCEF0AB3B7E.html"
                                                    class="flex items-center justify-center overflow-hidden w-full h-52 bg-gray-100 dark:bg-zinc-700">
                                                    <img class="object-contain max-h-52 lazy h-52 w-auto" width="200"
                                                        src="../../public/storage/site/placeholder/3A094BE8F9E5093D0205.webp"
                                                        data-src="https://riverr.edhub.me/public/storage/gigs/previews/small/D750250F547729B52624.jpg"
                                                        alt="Professionally edit and master your podcast in 24 hours">
                                                </a>


                                                <div class="px-4 pb-2 mt-4">


                                                    <div class="w-full mb-4 flex justify-between items-center">
                                                        <a href="../../profile/EdwardHendrix.html" target="_blank"
                                                            class="flex-shrink-0 group block">
                                                            <div class="flex items-center">
                                                                <span class="inline-block relative">
                                                                    <img class="h-6 w-6 rounded-full object-cover lazy"
                                                                        src="../../public/storage/site/placeholder/3A094BE8F9E5093D0205.webp"
                                                                        data-src="https://riverr.edhub.me/public/storage/avatars/BEF099D3385E2245EA89.png"
                                                                        alt="EdwardHendrix">
                                                                </span>
                                                                <div class="ltr:ml-3 rtl:mr-3">
                                                                    <div
                                                                        class="text-gray-500 dark:text-gray-200 hover:text-gray-900 dark:hover:text-gray-300 flex items-center mb-0.5 font-extrabold tracking-wide text-[13px]">
                                                                        EdwardHendrix
                                                                        <img data-tooltip-target="tooltip-account-verified-EF4353366FCEF0AB3B7E"
                                                                            class="ltr:ml-0.5 rtl:mr-0.5 h-4 w-4 -mt-0.5"
                                                                            src="../../public/img/auth/verified-badge.svg"
                                                                            alt="Account verified">
                                                                        <div id="tooltip-account-verified-EF4353366FCEF0AB3B7E"
                                                                            role="tooltip"
                                                                            class="inline-block absolute invisible z-10 py-2 px-3 text-xs font-medium text-white bg-gray-900 rounded-sm shadow-sm opacity-0 tooltip dark:bg-gray-700">
                                                                            Account verified
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>


                                                    <a href="../../service/professionally-edit-and-master-your-podcast-in-24-hours-EF4353366FCEF0AB3B7E.html"
                                                        class="gig-card-title font-semibold text-gray-800 dark:text-gray-100 hover:text-primary-600 dark:hover:text-white mb-4 !overflow-hidden">
                                                        Professionally edit and master your podcast in 24 hours
                                                    </a>


                                                    <div class="flex items-center" wire:ignore>


                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                            class="h-5 w-5 text-amber-400" viewBox="0 0 20 20"
                                                            fill="currentColor">
                                                            <path
                                                                d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                                                        </svg>


                                                        <div
                                                            class=" text-[13px] tracking-widest text-amber-500 ltr:ml-1 rtl:mr-1 font-black">
                                                            N/A</div>


                                                        <div
                                                            class="ltr:ml-2 rtl:mr-2 text-[13px] font-normal text-gray-400">
                                                            ( 0 )
                                                        </div>

                                                    </div>

                                                </div>


                                                <div
                                                    class="px-3 py-3 bg-[#fdfdfd] dark:bg-zinc-800 border-t border-gray-50 dark:border-zinc-700 text-right sm:px-4 rounded-b-md flex justify-between">

                                                    <div class="flex items-center">


                                                        <button x-on:click="loginToAddToFavorite"
                                                            data-tooltip-target="tooltip-add-to-favorites-EF4353366FCEF0AB3B7E"
                                                            class="h-8 w-8 rounded-full flex items-center justify-center -ml-1 focus:outline-none visited:outline-none">




                                                            <svg class="w-5 h-5 text-gray-400 hover:text-gray-500"
                                                                stroke="currentColor" fill="currentColor"
                                                                stroke-width="0" viewBox="0 0 20 20"
                                                                xmlns="http://www.w3.org/2000/svg">
                                                                <path fill-rule="evenodd"
                                                                    d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z"
                                                                    clip-rule="evenodd"></path>
                                                            </svg>

                                                        </button>
                                                        <div id="tooltip-add-to-favorites-EF4353366FCEF0AB3B7E"
                                                            role="tooltip"
                                                            class="inline-block absolute invisible z-10 py-2 px-3 text-xs font-medium text-white bg-gray-900 rounded-sm shadow-sm opacity-0 tooltip dark:bg-gray-700">
                                                            Add to favorite
                                                        </div>

                                                    </div>


                                                    <div class="gig-card-price">
                                                        <small class="text-body-3 dark:!text-zinc-200">Starting
                                                            at</small>
                                                        <span
                                                            class=" ltr:text-right rtl:text-left dark:!text-white">$40.00</span>
                                                    </div>

                                                </div>

                                            </div>

                                        </div>


                                        <!-- Livewire Component wire-end:NdAtZUj0MroUDgseO9la -->
                                    </div>



                                    <div class="col-span-12 lg:col-span-6 xl:col-span-4 md:col-span-6 sm:col-span-6"
                                        wire:key="gigs-list-90D256ECA9B77258AA9D">
                                        <div wire:id="5g91vl1OuxAX7LAWYrwc"
                                            wire:initial-data="{&quot;fingerprint&quot;:{&quot;id&quot;:&quot;5g91vl1OuxAX7LAWYrwc&quot;,&quot;name&quot;:&quot;main.cards.gig&quot;,&quot;locale&quot;:&quot;en&quot;,&quot;path&quot;:&quot;categories\/music-audio\/voice-over-streaming&quot;,&quot;method&quot;:&quot;GET&quot;,&quot;v&quot;:&quot;acj&quot;},&quot;effects&quot;:{&quot;listeners&quot;:[]},&quot;serverMemo&quot;:{&quot;children&quot;:[],&quot;errors&quot;:[],&quot;htmlHash&quot;:&quot;65eac99d&quot;,&quot;data&quot;:{&quot;gig&quot;:[],&quot;favorite&quot;:false,&quot;profile_visible&quot;:true},&quot;dataMeta&quot;:{&quot;models&quot;:{&quot;gig&quot;:{&quot;class&quot;:&quot;App\\Models\\Gig&quot;,&quot;id&quot;:217,&quot;relations&quot;:[&quot;thumbnail&quot;,&quot;owner&quot;,&quot;owner.avatar&quot;],&quot;connection&quot;:&quot;mysql&quot;,&quot;collectionClass&quot;:null}}},&quot;checksum&quot;:&quot;4f15ee61d2c9821ebe0594544e8f233d583666c55e893b94c4cbc93b951d0619&quot;}}"
                                            class="gig-card" x-data="window._90D256ECA9B77258AA9D" dir="ltr">
                                            <div
                                                class="bg-white dark:bg-zinc-800 rounded-md shadow-sm ring-1 ring-gray-200 dark:ring-zinc-800">


                                                <a href="../../service/edit-and-post-produce-your-podcast-90D256ECA9B77258AA9D.html"
                                                    class="flex items-center justify-center overflow-hidden w-full h-52 bg-gray-100 dark:bg-zinc-700">
                                                    <img class="object-contain max-h-52 lazy h-52 w-auto" width="200"
                                                        src="../../public/storage/site/placeholder/3A094BE8F9E5093D0205.webp"
                                                        data-src="https://riverr.edhub.me/public/storage/gigs/previews/small/3F100034B0C9DFA539B9.jpg"
                                                        alt="Edit, and post produce your podcast">
                                                </a>


                                                <div class="px-4 pb-2 mt-4">


                                                    <div class="w-full mb-4 flex justify-between items-center">
                                                        <a href="../../profile/EdwardHendrix.html" target="_blank"
                                                            class="flex-shrink-0 group block">
                                                            <div class="flex items-center">
                                                                <span class="inline-block relative">
                                                                    <img class="h-6 w-6 rounded-full object-cover lazy"
                                                                        src="../../public/storage/site/placeholder/3A094BE8F9E5093D0205.webp"
                                                                        data-src="https://riverr.edhub.me/public/storage/avatars/BEF099D3385E2245EA89.png"
                                                                        alt="EdwardHendrix">
                                                                </span>
                                                                <div class="ltr:ml-3 rtl:mr-3">
                                                                    <div
                                                                        class="text-gray-500 dark:text-gray-200 hover:text-gray-900 dark:hover:text-gray-300 flex items-center mb-0.5 font-extrabold tracking-wide text-[13px]">
                                                                        EdwardHendrix
                                                                        <img data-tooltip-target="tooltip-account-verified-90D256ECA9B77258AA9D"
                                                                            class="ltr:ml-0.5 rtl:mr-0.5 h-4 w-4 -mt-0.5"
                                                                            src="../../public/img/auth/verified-badge.svg"
                                                                            alt="Account verified">
                                                                        <div id="tooltip-account-verified-90D256ECA9B77258AA9D"
                                                                            role="tooltip"
                                                                            class="inline-block absolute invisible z-10 py-2 px-3 text-xs font-medium text-white bg-gray-900 rounded-sm shadow-sm opacity-0 tooltip dark:bg-gray-700">
                                                                            Account verified
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>


                                                    <a href="../../service/edit-and-post-produce-your-podcast-90D256ECA9B77258AA9D.html"
                                                        class="gig-card-title font-semibold text-gray-800 dark:text-gray-100 hover:text-primary-600 dark:hover:text-white mb-4 !overflow-hidden">
                                                        Edit, and post produce your podcast
                                                    </a>


                                                    <div class="flex items-center" wire:ignore>


                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                            class="h-5 w-5 text-amber-400" viewBox="0 0 20 20"
                                                            fill="currentColor">
                                                            <path
                                                                d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                                                        </svg>


                                                        <div
                                                            class=" text-[13px] tracking-widest text-amber-500 ltr:ml-1 rtl:mr-1 font-black">
                                                            N/A</div>


                                                        <div
                                                            class="ltr:ml-2 rtl:mr-2 text-[13px] font-normal text-gray-400">
                                                            ( 0 )
                                                        </div>

                                                    </div>

                                                </div>


                                                <div
                                                    class="px-3 py-3 bg-[#fdfdfd] dark:bg-zinc-800 border-t border-gray-50 dark:border-zinc-700 text-right sm:px-4 rounded-b-md flex justify-between">

                                                    <div class="flex items-center">


                                                        <button x-on:click="loginToAddToFavorite"
                                                            data-tooltip-target="tooltip-add-to-favorites-90D256ECA9B77258AA9D"
                                                            class="h-8 w-8 rounded-full flex items-center justify-center -ml-1 focus:outline-none visited:outline-none">




                                                            <svg class="w-5 h-5 text-gray-400 hover:text-gray-500"
                                                                stroke="currentColor" fill="currentColor"
                                                                stroke-width="0" viewBox="0 0 20 20"
                                                                xmlns="http://www.w3.org/2000/svg">
                                                                <path fill-rule="evenodd"
                                                                    d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z"
                                                                    clip-rule="evenodd"></path>
                                                            </svg>

                                                        </button>
                                                        <div id="tooltip-add-to-favorites-90D256ECA9B77258AA9D"
                                                            role="tooltip"
                                                            class="inline-block absolute invisible z-10 py-2 px-3 text-xs font-medium text-white bg-gray-900 rounded-sm shadow-sm opacity-0 tooltip dark:bg-gray-700">
                                                            Add to favorite
                                                        </div>

                                                    </div>


                                                    <div class="gig-card-price">
                                                        <small class="text-body-3 dark:!text-zinc-200">Starting
                                                            at</small>
                                                        <span
                                                            class=" ltr:text-right rtl:text-left dark:!text-white">$63.00</span>
                                                    </div>

                                                </div>

                                            </div>

                                        </div>


                                        <!-- Livewire Component wire-end:5g91vl1OuxAX7LAWYrwc -->
                                    </div>



                                    <div class="col-span-12 lg:col-span-6 xl:col-span-4 md:col-span-6 sm:col-span-6"
                                        wire:key="gigs-list-26335FD5E9AE60A8FDBC">
                                        <div wire:id="A5INcDmZzVz8JUsZb5i1"
                                            wire:initial-data="{&quot;fingerprint&quot;:{&quot;id&quot;:&quot;A5INcDmZzVz8JUsZb5i1&quot;,&quot;name&quot;:&quot;main.cards.gig&quot;,&quot;locale&quot;:&quot;en&quot;,&quot;path&quot;:&quot;categories\/music-audio\/voice-over-streaming&quot;,&quot;method&quot;:&quot;GET&quot;,&quot;v&quot;:&quot;acj&quot;},&quot;effects&quot;:{&quot;listeners&quot;:[]},&quot;serverMemo&quot;:{&quot;children&quot;:[],&quot;errors&quot;:[],&quot;htmlHash&quot;:&quot;c33f394c&quot;,&quot;data&quot;:{&quot;gig&quot;:[],&quot;favorite&quot;:false,&quot;profile_visible&quot;:true},&quot;dataMeta&quot;:{&quot;models&quot;:{&quot;gig&quot;:{&quot;class&quot;:&quot;App\\Models\\Gig&quot;,&quot;id&quot;:218,&quot;relations&quot;:[&quot;thumbnail&quot;,&quot;owner&quot;,&quot;owner.avatar&quot;],&quot;connection&quot;:&quot;mysql&quot;,&quot;collectionClass&quot;:null}}},&quot;checksum&quot;:&quot;85378d53273c1b25ce790a37b7fe1076dfe46e7506e9b25eb84925d3062b8dab&quot;}}"
                                            class="gig-card" x-data="window._26335FD5E9AE60A8FDBC" dir="ltr">
                                            <div
                                                class="bg-white dark:bg-zinc-800 rounded-md shadow-sm ring-1 ring-gray-200 dark:ring-zinc-800">


                                                <a href="../../service/be-your-italian-audiobook-narrator-26335FD5E9AE60A8FDBC.html"
                                                    class="flex items-center justify-center overflow-hidden w-full h-52 bg-gray-100 dark:bg-zinc-700">
                                                    <img class="object-contain max-h-52 lazy h-52 w-auto" width="200"
                                                        src="../../public/storage/site/placeholder/3A094BE8F9E5093D0205.webp"
                                                        data-src="https://riverr.edhub.me/public/storage/gigs/previews/small/30DCD0B70F42CB0139F1.jpg"
                                                        alt="Be your italian audiobook narrator">
                                                </a>


                                                <div class="px-4 pb-2 mt-4">


                                                    <div class="w-full mb-4 flex justify-between items-center">
                                                        <a href="../../profile/EdwardHendrix.html" target="_blank"
                                                            class="flex-shrink-0 group block">
                                                            <div class="flex items-center">
                                                                <span class="inline-block relative">
                                                                    <img class="h-6 w-6 rounded-full object-cover lazy"
                                                                        src="../../public/storage/site/placeholder/3A094BE8F9E5093D0205.webp"
                                                                        data-src="https://riverr.edhub.me/public/storage/avatars/BEF099D3385E2245EA89.png"
                                                                        alt="EdwardHendrix">
                                                                </span>
                                                                <div class="ltr:ml-3 rtl:mr-3">
                                                                    <div
                                                                        class="text-gray-500 dark:text-gray-200 hover:text-gray-900 dark:hover:text-gray-300 flex items-center mb-0.5 font-extrabold tracking-wide text-[13px]">
                                                                        EdwardHendrix
                                                                        <img data-tooltip-target="tooltip-account-verified-26335FD5E9AE60A8FDBC"
                                                                            class="ltr:ml-0.5 rtl:mr-0.5 h-4 w-4 -mt-0.5"
                                                                            src="../../public/img/auth/verified-badge.svg"
                                                                            alt="Account verified">
                                                                        <div id="tooltip-account-verified-26335FD5E9AE60A8FDBC"
                                                                            role="tooltip"
                                                                            class="inline-block absolute invisible z-10 py-2 px-3 text-xs font-medium text-white bg-gray-900 rounded-sm shadow-sm opacity-0 tooltip dark:bg-gray-700">
                                                                            Account verified
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>


                                                    <a href="../../service/be-your-italian-audiobook-narrator-26335FD5E9AE60A8FDBC.html"
                                                        class="gig-card-title font-semibold text-gray-800 dark:text-gray-100 hover:text-primary-600 dark:hover:text-white mb-4 !overflow-hidden">
                                                        Be your italian audiobook narrator
                                                    </a>


                                                    <div class="flex items-center" wire:ignore>


                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                            class="h-5 w-5 text-amber-400" viewBox="0 0 20 20"
                                                            fill="currentColor">
                                                            <path
                                                                d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                                                        </svg>


                                                        <div
                                                            class=" text-[13px] tracking-widest text-amber-500 ltr:ml-1 rtl:mr-1 font-black">
                                                            N/A</div>


                                                        <div
                                                            class="ltr:ml-2 rtl:mr-2 text-[13px] font-normal text-gray-400">
                                                            ( 0 )
                                                        </div>

                                                    </div>

                                                </div>


                                                <div
                                                    class="px-3 py-3 bg-[#fdfdfd] dark:bg-zinc-800 border-t border-gray-50 dark:border-zinc-700 text-right sm:px-4 rounded-b-md flex justify-between">

                                                    <div class="flex items-center">


                                                        <button x-on:click="loginToAddToFavorite"
                                                            data-tooltip-target="tooltip-add-to-favorites-26335FD5E9AE60A8FDBC"
                                                            class="h-8 w-8 rounded-full flex items-center justify-center -ml-1 focus:outline-none visited:outline-none">




                                                            <svg class="w-5 h-5 text-gray-400 hover:text-gray-500"
                                                                stroke="currentColor" fill="currentColor"
                                                                stroke-width="0" viewBox="0 0 20 20"
                                                                xmlns="http://www.w3.org/2000/svg">
                                                                <path fill-rule="evenodd"
                                                                    d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z"
                                                                    clip-rule="evenodd"></path>
                                                            </svg>

                                                        </button>
                                                        <div id="tooltip-add-to-favorites-26335FD5E9AE60A8FDBC"
                                                            role="tooltip"
                                                            class="inline-block absolute invisible z-10 py-2 px-3 text-xs font-medium text-white bg-gray-900 rounded-sm shadow-sm opacity-0 tooltip dark:bg-gray-700">
                                                            Add to favorite
                                                        </div>

                                                    </div>


                                                    <div class="gig-card-price">
                                                        <small class="text-body-3 dark:!text-zinc-200">Starting
                                                            at</small>
                                                        <span
                                                            class=" ltr:text-right rtl:text-left dark:!text-white">$17.00</span>
                                                    </div>

                                                </div>

                                            </div>

                                        </div>


                                        <!-- Livewire Component wire-end:A5INcDmZzVz8JUsZb5i1 -->
                                    </div>



                                    <div class="col-span-12 lg:col-span-6 xl:col-span-4 md:col-span-6 sm:col-span-6"
                                        wire:key="gigs-list-1E0A27D3990BC78E1310">
                                        <div wire:id="KIQ6on6YuF0C6Jn8HoBc"
                                            wire:initial-data="{&quot;fingerprint&quot;:{&quot;id&quot;:&quot;KIQ6on6YuF0C6Jn8HoBc&quot;,&quot;name&quot;:&quot;main.cards.gig&quot;,&quot;locale&quot;:&quot;en&quot;,&quot;path&quot;:&quot;categories\/music-audio\/voice-over-streaming&quot;,&quot;method&quot;:&quot;GET&quot;,&quot;v&quot;:&quot;acj&quot;},&quot;effects&quot;:{&quot;listeners&quot;:[]},&quot;serverMemo&quot;:{&quot;children&quot;:[],&quot;errors&quot;:[],&quot;htmlHash&quot;:&quot;2563edaf&quot;,&quot;data&quot;:{&quot;gig&quot;:[],&quot;favorite&quot;:false,&quot;profile_visible&quot;:true},&quot;dataMeta&quot;:{&quot;models&quot;:{&quot;gig&quot;:{&quot;class&quot;:&quot;App\\Models\\Gig&quot;,&quot;id&quot;:219,&quot;relations&quot;:[&quot;thumbnail&quot;,&quot;owner&quot;,&quot;owner.avatar&quot;],&quot;connection&quot;:&quot;mysql&quot;,&quot;collectionClass&quot;:null}}},&quot;checksum&quot;:&quot;6c3780e25076e13581cacf48c4fbb550a45b3ec23ea9f05d5932d32d449a17a5&quot;}}"
                                            class="gig-card" x-data="window._1E0A27D3990BC78E1310" dir="ltr">
                                            <div
                                                class="bg-white dark:bg-zinc-800 rounded-md shadow-sm ring-1 ring-gray-200 dark:ring-zinc-800">


                                                <a href="../../service/be-your-male-british-commercial-voice-over-1E0A27D3990BC78E1310.html"
                                                    class="flex items-center justify-center overflow-hidden w-full h-52 bg-gray-100 dark:bg-zinc-700">
                                                    <img class="object-contain max-h-52 lazy h-52 w-auto" width="200"
                                                        src="../../public/storage/site/placeholder/3A094BE8F9E5093D0205.webp"
                                                        data-src="https://riverr.edhub.me/public/storage/gigs/previews/small/4E6156E66ADB91F85D06.jpg"
                                                        alt="Be your male british commercial voice over">
                                                </a>


                                                <div class="px-4 pb-2 mt-4">


                                                    <div class="w-full mb-4 flex justify-between items-center">
                                                        <a href="../../profile/EdwardHendrix.html" target="_blank"
                                                            class="flex-shrink-0 group block">
                                                            <div class="flex items-center">
                                                                <span class="inline-block relative">
                                                                    <img class="h-6 w-6 rounded-full object-cover lazy"
                                                                        src="../../public/storage/site/placeholder/3A094BE8F9E5093D0205.webp"
                                                                        data-src="https://riverr.edhub.me/public/storage/avatars/BEF099D3385E2245EA89.png"
                                                                        alt="EdwardHendrix">
                                                                </span>
                                                                <div class="ltr:ml-3 rtl:mr-3">
                                                                    <div
                                                                        class="text-gray-500 dark:text-gray-200 hover:text-gray-900 dark:hover:text-gray-300 flex items-center mb-0.5 font-extrabold tracking-wide text-[13px]">
                                                                        EdwardHendrix
                                                                        <img data-tooltip-target="tooltip-account-verified-1E0A27D3990BC78E1310"
                                                                            class="ltr:ml-0.5 rtl:mr-0.5 h-4 w-4 -mt-0.5"
                                                                            src="../../public/img/auth/verified-badge.svg"
                                                                            alt="Account verified">
                                                                        <div id="tooltip-account-verified-1E0A27D3990BC78E1310"
                                                                            role="tooltip"
                                                                            class="inline-block absolute invisible z-10 py-2 px-3 text-xs font-medium text-white bg-gray-900 rounded-sm shadow-sm opacity-0 tooltip dark:bg-gray-700">
                                                                            Account verified
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>


                                                    <a href="../../service/be-your-male-british-commercial-voice-over-1E0A27D3990BC78E1310.html"
                                                        class="gig-card-title font-semibold text-gray-800 dark:text-gray-100 hover:text-primary-600 dark:hover:text-white mb-4 !overflow-hidden">
                                                        Be your male british commercial voice over
                                                    </a>


                                                    <div class="flex items-center" wire:ignore>


                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                            class="h-5 w-5 text-amber-400" viewBox="0 0 20 20"
                                                            fill="currentColor">
                                                            <path
                                                                d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                                                        </svg>


                                                        <div
                                                            class=" text-[13px] tracking-widest text-amber-500 ltr:ml-1 rtl:mr-1 font-black">
                                                            N/A</div>


                                                        <div
                                                            class="ltr:ml-2 rtl:mr-2 text-[13px] font-normal text-gray-400">
                                                            ( 0 )
                                                        </div>

                                                    </div>

                                                </div>


                                                <div
                                                    class="px-3 py-3 bg-[#fdfdfd] dark:bg-zinc-800 border-t border-gray-50 dark:border-zinc-700 text-right sm:px-4 rounded-b-md flex justify-between">

                                                    <div class="flex items-center">


                                                        <button x-on:click="loginToAddToFavorite"
                                                            data-tooltip-target="tooltip-add-to-favorites-1E0A27D3990BC78E1310"
                                                            class="h-8 w-8 rounded-full flex items-center justify-center -ml-1 focus:outline-none visited:outline-none">




                                                            <svg class="w-5 h-5 text-gray-400 hover:text-gray-500"
                                                                stroke="currentColor" fill="currentColor"
                                                                stroke-width="0" viewBox="0 0 20 20"
                                                                xmlns="http://www.w3.org/2000/svg">
                                                                <path fill-rule="evenodd"
                                                                    d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z"
                                                                    clip-rule="evenodd"></path>
                                                            </svg>

                                                        </button>
                                                        <div id="tooltip-add-to-favorites-1E0A27D3990BC78E1310"
                                                            role="tooltip"
                                                            class="inline-block absolute invisible z-10 py-2 px-3 text-xs font-medium text-white bg-gray-900 rounded-sm shadow-sm opacity-0 tooltip dark:bg-gray-700">
                                                            Add to favorite
                                                        </div>

                                                    </div>


                                                    <div class="gig-card-price">
                                                        <small class="text-body-3 dark:!text-zinc-200">Starting
                                                            at</small>
                                                        <span
                                                            class=" ltr:text-right rtl:text-left dark:!text-white">$39.00</span>
                                                    </div>

                                                </div>

                                            </div>

                                        </div>


                                        <!-- Livewire Component wire-end:KIQ6on6YuF0C6Jn8HoBc -->
                                    </div>




                                </div>
                 </div>
            </div>
        </section>
    </main>
</div>

<?php $__env->startPush('scripts'); ?>

    
    <script>
        function xRiqSKhIlmnKtWu() {
            return {

                open: false,

                // Init component
                initialize() {
                    
                }

            }
        }
        window.xRiqSKhIlmnKtWu = xRiqSKhIlmnKtWu();
    </script>

<?php $__env->stopPush(); ?><?php /**PATH /home/u723366549/domains/payperwork.in/public_html/resources/views/livewire/posting/listing.blade.php ENDPATH**/ ?>